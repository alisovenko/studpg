package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"
	"os"

	_ "github.com/lib/pq"
)
func main() {
	connStr := os.Getenv("DATABASE_URL")
	if connStr == "" {
		log.Fatal("DATABASE_URL is not set")
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()


	ctx := context.Background()

	for {
		rows, err := db.QueryContext(ctx, "SELECT * FROM auto")
		if err != nil {
			log.Fatal(err)
		}
		defer rows.Close()

		rowsPresent := false
		for rows.Next() {
			var id int
			var vin string
			if err := rows.Scan(&id, &vin); err != nil {
				log.Fatal(err)
			}
			fmt.Printf("ID: %d, VIN: %s\n", id, vin)
			rowsPresent = true
		}
		if err := rows.Err(); err != nil {
			log.Fatal(err)
		}

		if !rowsPresent{
			fmt.Println("No rows\n")
		}
		time.Sleep(time.Second)

	}
}