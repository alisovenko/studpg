-- +goose Up
-- SQL in this section is executed when the migration is applied.
CREATE TABLE auto (id  bigserial, vin text);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE auto;