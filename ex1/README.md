# Уровни изоляции


## Run pg
```sh
docker-compose up -d
```

## Create init data
```
psql -h 127.0.0.1 -U postgres db -f migrations.sql 
```

## Experiments

Start session
```
psql -h 127.0.0.1 -U postgres db
```

### Read uncommited (level)
| ses1 | ses2 |
| ----------- | ----------- |
|show transaction isolation level;|show transaction isolation level;|
|**>** read committed|**>** read committed|
|begin;|begin;|
|**>** BEGIN|**>** BEGIN|
|insert into persons(first_name, second_name) values('sergey', 'sergeev');||
|**>** INSERT 0 1||
|| select * from persons where first_name = 'sergey';|
||**>** (0 rows)|
||`грязное чтение НЕ допускается`||
|commit;||
|**>** COMMIT||
|| select * from persons where first_name = 'sergey';|
||**>** (1 row)|
||`фантомное чтение допускается`||
||rollback;|
||**>** ROLLBACK||

### Repeatable read (level)
| ses1 | ses2 |
| ----------- | ----------- |
|begin;|begin;|
|**>** BEGIN|**>** BEGIN|
|set transaction isolation level repeatable read;|set transaction isolation level repeatable read;|
|**>** SET|**>** SET|
|insert into persons(first_name, second_name) values('sveta', 'svetova');||
|**>** INSERT 0 1||
|| select * from persons where first_name = 'sveta';|
||**>** (0 rows)|
||`грязное чтение НЕ допускается`||
|commit;||
|**>** COMMIT||
|| select * from persons where first_name = 'sveta';|
||**>** (0 rows)|
||`фантомное чтение НЕ допускается`||
||rollback;|
||**>** ROLLBACK||
|| select * from persons where first_name = 'sveta';|
||**>** (1 rows)|
||`новая транзакция имеет доступ ко всем закоммиченным даннным`||


### Nonrepeatable Read (phenomena)
| ses1 | ses2 |
| ----------- | ----------- |
|show transaction isolation level;|show transaction isolation level;|
|**>** read committed|**>** read committed|
|begin;|begin;|
|**>** BEGIN|**>** BEGIN|
||select first_name from persons where id =1;|
||**>** ivan |
|update persons set first_name='Ivan' where id = 1;||
|**>** UPDATE 1||
|commit;||
|**>** COMMIT||
||select first_name from persons where id =1;|
||**>** Ivan |
||`Неповторяемое чтение возможно при read committed`|

| ses1 | ses2 |
| ----------- | ----------- |
|begin;|begin;|
|**>** BEGIN|**>** BEGIN|
|set transaction isolation level repeatable read;|set transaction isolation level repeatable read;|
|**>** SET|**>** SET|
||select first_name from persons where id =2;|
||**>** petr |
|update persons set first_name='Petr' where id = 2;||
|**>** UPDATE 1||
|commit;||
|**>** COMMIT||
||select first_name from persons where id = 2;|
||**>** petr |
||`Неповторяемое чтение НЕ возможно при repeatable read`|