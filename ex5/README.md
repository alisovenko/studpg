# Создал VM (ynadex.cloud)
Платформа -Intel Ice Lake  
Гарантированная доля vCPU - 100%  
​vCPU - 2  
RAM - 4 ГБ  
Объём дискового пространства - 10 ГБ  

# Подготовка
```
sudo apt install docker-compose
git clone https://gitlab.com/alisovenko/studpg.git 
cd studpg/ex5
```

# Запуск PG с разными параметрами

## Запсук PG 
```
docker-compose up db1 bd2
```
запускаю два инстанса, с базами: db1, db2  
порты "вовне" соответственно: 5431, 5432  

для db1 - параметры остаются по-умолчанию  
для db2 - параметры меняю на предложенные в задании  

параметры меняю в файле postgresql.conf с перезапуском контейнеров  
(том с файлами БД смонтирован в определенную диреторию поэтому измения видны при перезапуске)

## Запуск pgbench (с локального ПК)
### тест инстанса с параметрами по-умолчанию (db1)
```
pgbench -h 84.201.130.9 -U postgres -p 5431 -i db1
pgbench -c8 -P 6 -T 60 -h 84.201.130.9 -p 5431 -U postgres db1
```
результаты:
```
pgbench (15.5 (Debian 15.5-1.pgdg120+1), server 15.6 (Debian 15.6-1.pgdg120+2))
starting vacuum...end.
progress: 6.0 s, 4.7 tps, lat 869.148 ms stddev 419.919, 0 failed
progress: 12.0 s, 9.0 tps, lat 893.128 ms stddev 273.546, 0 failed
progress: 18.0 s, 9.0 tps, lat 881.905 ms stddev 274.537, 0 failed
progress: 24.0 s, 8.8 tps, lat 886.077 ms stddev 371.587, 0 failed
progress: 30.0 s, 9.0 tps, lat 909.462 ms stddev 288.614, 0 failed
progress: 36.0 s, 9.0 tps, lat 891.616 ms stddev 263.088, 0 failed
progress: 42.0 s, 9.0 tps, lat 878.789 ms stddev 333.393, 0 failed
progress: 48.0 s, 9.0 tps, lat 892.140 ms stddev 315.224, 0 failed
progress: 54.0 s, 8.7 tps, lat 918.742 ms stddev 358.317, 0 failed
progress: 60.0 s, 9.0 tps, lat 886.467 ms stddev 391.973, 0 failed
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 8
number of threads: 1
maximum number of tries: 1
duration: 60 s
number of transactions actually processed: 519
number of failed transactions: 0 (0.000%)
latency average = 893.472 ms
latency stddev = 329.353 ms
initial connection time = 2518.538 ms
tps = 8.885010 (without initial connection time)
```

### тест инстанса с предложенными параметрами (db2)
```
pgbench -h 84.201.130.9 -U postgres -p 5432 -i db2
pgbench -c8 -P 6 -T 60 -h 84.201.130.9 -p 5432 -U postgres db2
```
результаты:
```
pgbench (15.5 (Debian 15.5-1.pgdg120+1), server 15.6 (Debian 15.6-1.pgdg120+2))
starting vacuum...end.
progress: 6.0 s, 4.8 tps, lat 850.651 ms stddev 371.532, 0 failed
progress: 12.0 s, 9.0 tps, lat 884.169 ms stddev 267.340, 0 failed
progress: 18.0 s, 9.2 tps, lat 884.964 ms stddev 387.304, 0 failed
progress: 24.0 s, 9.0 tps, lat 875.416 ms stddev 213.320, 0 failed
progress: 30.0 s, 9.0 tps, lat 891.358 ms stddev 284.092, 0 failed
progress: 36.0 s, 8.8 tps, lat 911.493 ms stddev 272.562, 0 failed
progress: 42.0 s, 9.2 tps, lat 874.043 ms stddev 330.056, 0 failed
progress: 48.0 s, 9.0 tps, lat 888.341 ms stddev 312.808, 0 failed
progress: 54.0 s, 9.2 tps, lat 883.245 ms stddev 280.094, 0 failed
progress: 60.0 s, 9.0 tps, lat 876.540 ms stddev 312.632, 0 failed
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 8
number of threads: 1
maximum number of tries: 1
duration: 60 s
number of transactions actually processed: 525
number of failed transactions: 0 (0.000%)
latency average = 883.484 ms
latency stddev = 303.347 ms
initial connection time = 2417.974 ms
tps = 8.994518 (without initial connection time)
```

## Запуск pgbench (внутри контейнера, рядом с БД)
### тест инстанса с параметрами по-умолчанию (db1)
```
pgbench -h 127.0.0.1 -U postgres -i db1
pgbench -c8 -P 6 -T 60 -h 127.0.0.1 -U postgres db1
```
результаты:
```
pgbench (15.6 (Debian 15.6-1.pgdg120+2))
starting vacuum...end.
progress: 6.0 s, 541.7 tps, lat 14.707 ms stddev 10.325, 0 failed
progress: 12.0 s, 530.2 tps, lat 15.064 ms stddev 11.774, 0 failed
progress: 18.0 s, 622.2 tps, lat 12.862 ms stddev 7.851, 0 failed
progress: 24.0 s, 541.2 tps, lat 14.760 ms stddev 9.711, 0 failed
progress: 30.0 s, 397.5 tps, lat 20.130 ms stddev 20.545, 0 failed
progress: 36.0 s, 584.8 tps, lat 13.649 ms stddev 8.307, 0 failed
progress: 42.0 s, 515.8 tps, lat 15.511 ms stddev 10.240, 0 failed
progress: 48.0 s, 520.2 tps, lat 15.343 ms stddev 10.242, 0 failed
progress: 54.0 s, 460.7 tps, lat 17.368 ms stddev 12.812, 0 failed
progress: 60.0 s, 279.7 tps, lat 28.501 ms stddev 30.863, 0 failed
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 8
number of threads: 1
maximum number of tries: 1
duration: 60 s
number of transactions actually processed: 29971
number of failed transactions: 0 (0.000%)
latency average = 16.006 ms
latency stddev = 13.757 ms
initial connection time = 14.839 ms
tps = 499.424748 (without initial connection time)

```

### тест инстанса с предложенными параметрами (db2)
```
pgbench -h 127.0.0.1 -U postgres -i db2
pgbench -c8 -P 6 -T 60 -h 127.0.0.1 -U postgres db2
```
результаты:
```
pgbench (15.6 (Debian 15.6-1.pgdg120+2))
starting vacuum...end.
progress: 6.0 s, 562.2 tps, lat 14.170 ms stddev 10.043, 0 failed
progress: 12.0 s, 595.3 tps, lat 13.181 ms stddev 10.739, 0 failed
progress: 18.0 s, 259.7 tps, lat 31.367 ms stddev 34.181, 0 failed
progress: 24.0 s, 523.2 tps, lat 15.277 ms stddev 10.735, 0 failed
progress: 30.0 s, 558.5 tps, lat 14.316 ms stddev 10.649, 0 failed
progress: 36.0 s, 509.0 tps, lat 15.706 ms stddev 9.565, 0 failed
progress: 42.0 s, 518.7 tps, lat 15.344 ms stddev 13.204, 0 failed
progress: 48.0 s, 426.8 tps, lat 18.821 ms stddev 21.225, 0 failed
progress: 54.0 s, 629.0 tps, lat 12.669 ms stddev 8.022, 0 failed
progress: 60.0 s, 576.7 tps, lat 13.905 ms stddev 10.059, 0 failed
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 8
number of threads: 1
maximum number of tries: 1
duration: 60 s
number of transactions actually processed: 30962
number of failed transactions: 0 (0.000%)
latency average = 15.491 ms
latency stddev = 14.347 ms
initial connection time = 14.867 ms
tps = 516.019298 (without initial connection time)
```

## Итоги
Мне кажется, что особой разницы нет.  
Возможно, профиль нагрузки не так сильно нагружает БД в части тех характеристик что мы настраивали.  

|параметр|изменил на| по-умолчанию|
| ----------- | ----------- | ----------- |
|max_connections | 40 | было 100 |
|shared_buffers | 1GB | было 128MB |
|effective_cache_size | 3GB | было закоментировано |
|maintenance_work_mem | 512MB | было закоментировано |
|checkpoint_completion_target | 0.9 | было закоментировано |
|wal_buffers | 16MB | было закоментировано |
|default_statistics_target | 500 | было закоментировано |
|random_page_cost | 4 | было закоментировано |
|effective_io_concurrency | 2 | было закоментировано |
|work_mem | 6553kB | было закоментировано |
|min_wal_size | 4GB | 1GB |
|max_wal_size | 16GB | 80Mb |

Например,  
```max_connections = 40```  
, вроде, никак не вляет на наш тест, вообще достаточно 8, т.к. у нас в тесте 8 паралельных клиентов.  
А вот если указать меньше 8, то pgbench не запустится, т.к. не удасться создать нужное кол-во подключений.  


Однако, если установить значение ```work_mem=64kB```, то в тесте заметно снижается tps где-то на 10%.


# VACUUM
## Запсук PG 
```
docker-compose up db3
```
запускаю инстанс с базой: test

## Создаем табличку, заполняем и смотрим размер
```sql
CREATE TABLE person(name text);
INSERT INTO person(name) SELECT 'name_'||generate_series FROM generate_series(1,1000000);
SELECT pg_size_pretty(pg_total_relation_size('person'));
```

Размер таблицы: 42 Mb  

### Смотрим на мертвые записи
```sql

UPDATE person SET name='x'||name; -- повторяю 5 раз
SELECT relname, n_live_tup, n_dead_tup, last_autovacuum FROM pg_stat_user_TABLEs WHERE relname = 'person';
```

Результат:  
|relname|n_live_tup|n_dead_tup|last_autovacuum|
|-|-|-|-|
|person  |    1000000 |    2999647 | 2024-03-10 11:03:05.851432+00|

Через минуту (автовакум отработал очередной раз) "мертвых" записей уже нет:
|relname|n_live_tup|n_dead_tup|last_autovacuum|
|-|-|-|-|
|person  |    1000000 |    0 | 2024-03-10 11:04:06.759585+00|


### Смотрим на размер таблицы
```sql

UPDATE person SET name='y'||name; -- повторяю 5 раз
SELECT pg_size_pretty(pg_total_relation_size('person'));
```

Итоговое значение поля name:
```
# select * from person limit 1;
yyyyyxxxxxname_848157
```

Размер таблицы: 249 MB  
**да, данные копятся**  


### Отключаем автовакуум
```sql
ALTER TABLE person SET (autovacuum_enabled = off);

DO $$
DECLARE
    i int := 1;
BEGIN
    RAISE NOTICE 'START person updating';
    WHILE i <= 10 LOOP
        RAISE NOTICE 'Update person №%', i;
        UPDATE person SET name='z'||name;
        i := i + 1;
    END LOOP;
    RAISE NOTICE 'FINISH person updating';
END;
$$;


SELECT pg_size_pretty(pg_total_relation_size('person'));
```
Размер таблицы: 608 MB

| Размер таблицы увеличился больше чем в два раза.  
| Размер увеличился т.к. данных стало больше чем в два раза: старые вырсии строк не удалялись, новые версии строк добавились и они больше чем старые.  
| Размер не увеличился бы так сильно, если бы работал автовакуум, и на место освобожденных (автовакумом) записей были помещены их новые версии.

Кол-во "мертвых" строк = 10 000 000

### Дополнительный эксперимент

```sql
vacuum verbose person;
```

Размер таблицы не поменялся - т.к. это не vacuum full.  
Но я ожидаю, что при очередном обновлении, размер не увеличится,  
т.к. будет использовано место, освобожденное вакумом.  

```sql
DO $$
DECLARE
    i int := 1;
BEGIN
    RAISE NOTICE 'START person updating';
    WHILE i <= 5 LOOP
        RAISE NOTICE 'Update person №%', i;
        UPDATE person SET name='w'||name;
        i := i + 1;
    END LOOP;
    RAISE NOTICE 'FINISH person updating';
END;
$$;

SELECT pg_size_pretty(pg_total_relation_size('person'));
```

**Да, так и есть, размер остался 608 MB**