# Логический уровень PostgreSQL 

## Придумываем БД
```sql
-- стркутура "онлайн-магазина"

-- данный о товарах, покупателях, карзинах
CREATE SCHEMA shopping;

-- данные о доставке посылок
CREATE SCHEMA delivery;

-- все пользователи магазина
CREATE TABLE person(
    id serial,
    first_name text,
    second_name text
);
insert into person(first_name, second_name) values('ivan', 'ivanov'); 
insert into person(first_name, second_name) values('petr', 'petrov');

-- корзины покупателей
CREATE TABLE shopping.cart(
    id serial,
    customer int --person_id
);
insert into shopping.cart (customer) values(1); 


-- посылки
CREATE TABLE delivery.posting(
    id serial,
    seller int --person_id
);
insert into delivery.posting (seller) values(2);

-- склады
CREATE TABLE delivery.warehouse(
    id serial,
    address text
);
insert into delivery.warehouse (address) values ('abcdef');
```

## Роли, которые понадобятся
```sql
-- доступно только чтение с таблицы person
CREATE ROLE persons_reader;
GRANT CONNECT ON DATABASE marketplace TO persons_reader;
GRANT SELECT ON person TO persons_reader;

-- доступно только чтение со всех таблиц схемы delivery
CREATE ROLE delivering_reader;
GRANT CONNECT ON DATABASE marketplace TO delivering_reader;
GRANT USAGE ON SCHEMA delivery TO delivering_reader;
GRANT SELECT ON ALL TABLES in SCHEMA delivery TO delivering_reader;

-- доступно только чтение со всех таблиц схемы shopping
CREATE ROLE shopping_reader;
GRANT CONNECT ON DATABASE marketplace TO shopping_reader;
GRANT USAGE ON SCHEMA shopping TO shopping_reader;
GRANT SELECT ON ALL TABLES in SCHEMA shopping TO shopping_reader;
```

## Пользователи и их возможности
### Пользователь, которому доступны только пользователи магазина для чтения
```sql
-- аналитик по пользователям магазина
CREATE USER persons_analyst WITH PASSWORD '123';
GRANT persons_reader TO persons_analyst;
```

Выполняя из под persons_analyst, видим что данные доступны для чтения из таблицы person
```
marketplace=> select * from person;
 id | first_name | second_name 
----+------------+-------------
  1 | ivan       | ivanov
  2 | petr       | petrov
(2 rows)
```
, **а модифицирование данных не доступно!**
```
marketplace=> delete from person;
ERROR:  permission denied for table person
```

Выполняя из под persons_analyst, видим что другие данные **не доступны**
```
marketplace=> select * from shopping.cart;
ERROR:  permission denied for schema shopping
LINE 1: select * from shopping.cart;
```

### Пользователь, которому доступны только данные из раздела delivery магазина
```sql
-- аналитик по доставке товаров
CREATE USER delivering_analyst WITH PASSWORD '123';
GRANT delivering_reader TO delivering_analyst;
```

Выполняя из под delivering_analyst, видим что данные доступны для чтения из таблиц схемы delivery
```
marketplace=> select * from delivery.posting;
 id | seller 
----+--------
  1 |      2
(1 row)
```
```
marketplace=> select * from delivery.warehouse;
 id | address 
----+---------
  1 | abcdef
(1 row)
```

### Пользователь, которому доступны только данные из раздела shopping магазина
```sql
-- аналитик по покупателям
CREATE USER shopping_analyst WITH PASSWORD '123';
GRANT shopping_reader TO shopping_analyst;
```

Выполняя из под shopping_analyst, видим что данные доступны для чтения из таблиц схемы shopping
```
marketplace=> select * from delivery.posting;
 id | seller 
----+--------
  1 |      2
(1 row)
```

Но, в процессе жизни магазина добавляется еще таблица:
```sql
CREATE TABLE shopping.discount(
    id serial,
    customer int, --person_id
    amount int
);
insert into shopping.discount (customer, amount) values(1, 10); 
```

**Новые данные в той же схеме не доступны!**
```
marketplace=> select * from shopping.discount;
ERROR:  permission denied for table discount
```

Видимо при назначении прав на схему - даются права на все таблицы этой схемы в момент назначения.  
Повторяем разрешение для роли:
```sql
GRANT SELECT ON ALL TABLES in SCHEMA shopping TO shopping_reader;
```

Успешно получаем данные:
```
marketplace=> select * from shopping.discount;
 id | customer | amount 
----+----------+--------
  1 |        1 |     10
(1 row)
```


## Оказалось, пользователь shopping_analyst может больше
Пользователь смог создать таблицу в public
```
marketplace=> create table t(c integer);
CREATE TABLE
```

Пользователь может создавать таблице в public
```
marketplace=> select has_schema_privilege('public', 'CREATE');
 has_schema_privilege 
----------------------
 t
(1 row)
```

Пробуем запретить:
```
REVOKE CREATE ON SCHEMA public FROM shopping_analyst;
```

- **так НЕ получилось отозвать это право**, пользователь все равно может создавать тоблицы
причину понять пока не могу

Получилось вот так:
```sql
REVOKE CREATE ON SCHEMA public FROM PUBLIC;
```
, т.е. отзыв у всех кроме суперюзеров
