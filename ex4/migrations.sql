-- стркутура "онлайн-магазина"

-- данный о товарах, покупателях, карзинах
CREATE SCHEMA shopping;

-- данные о доставке посылок
CREATE SCHEMA delivery;

-- все пользователи магазина
CREATE TABLE person(
    id serial,
    first_name text,
    second_name text
);
insert into person(first_name, second_name) values('ivan', 'ivanov'); 
insert into person(first_name, second_name) values('petr', 'petrov');

-- корзины покупателей
CREATE TABLE shopping.cart(
    id serial,
    customer int --person_id
);
insert into shopping.cart (customer) values(1); 


-- посылки
CREATE TABLE delivery.posting(
    id serial,
    seller int --person_id
);
insert into delivery.posting (seller) values(2);

-- склады
CREATE TABLE delivery.warehouse(
    id serial,
    address text
);
insert into delivery.warehouse (address) values ('abcdef');

-- роли
-- доступно только чтение с таблицы person
CREATE ROLE persons_reader;
GRANT CONNECT ON DATABASE marketplace TO persons_reader;
GRANT SELECT ON person TO persons_reader;

-- доступно только чтение со всех таблиц схемы delivery
CREATE ROLE delivering_reader;
GRANT CONNECT ON DATABASE marketplace TO delivering_reader;
GRANT USAGE ON SCHEMA delivery TO delivering_reader;
GRANT SELECT ON ALL TABLES in SCHEMA delivery TO delivering_reader;

-- доступно только чтение со всех таблиц схемы shopping
CREATE ROLE shopping_reader;
GRANT CONNECT ON DATABASE marketplace TO shopping_reader;
GRANT USAGE ON SCHEMA shopping TO shopping_reader;
GRANT SELECT ON ALL TABLES in SCHEMA shopping TO shopping_reader;


-- пользователи БД
-- аналитик по пользователям магащина
CREATE USER persons_analyst WITH PASSWORD '123';
GRANT persons_reader TO persons_analyst;

-- аналитик по доставке
CREATE USER delivery_analyst WITH PASSWORD '123';
GRANT delivering_reader TO delivery_analyst;

-- аналитик по покупателям
CREATE USER shopping_analyst WITH PASSWORD '123';
GRANT shopping_reader TO shopping_analyst;

