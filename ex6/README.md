## WAL

### Настройки
включаю создание контрольных точек раз в 30 сек:
```
alter system set checkpoint_timeout = '30s';
```

по заданию нужно будет проверить время выполнения контрольных точек,  
я нашел только такой способ - посмотреть в логах  
**вопрос 1: верно ли это? или есть другой способ?**

поэтому включаю еще логи 
```
log_checkpoints
```

```
log_destination = 'stderr'
logging_collector = on
log_directory = 'log'
log_filename = 'postgresql-%Y-%m-%d_%H%M%S.log'
log_rotation_size = 100MB
```


### 10 мин. нагрузки
```
root@42a43aee5698:/# pgbench -h 127.0.0.1 -U postgres -i db
root@42a43aee5698:/# pgbench -c8 -P 6 -T 600 -h 127.0.0.1 -U postgres db
pgbench (15.6 (Debian 15.6-1.pgdg120+2))
starting vacuum...end.
progress: 6.0 s, 584.3 tps, lat 13.636 ms stddev 13.357, 0 failed
progress: 12.0 s, 683.2 tps, lat 11.681 ms stddev 7.135, 0 failed
...
progress: 594.0 s, 519.8 tps, lat 15.366 ms stddev 9.196, 0 failed
progress: 600.0 s, 484.8 tps, lat 16.469 ms stddev 14.368, 0 failed
transaction type: <builtin: TPC-B (sort of)>
scaling factor: 1
query mode: simple
number of clients: 8
number of threads: 1
maximum number of tries: 1
duration: 600 s
number of transactions actually processed: 402917
number of failed transactions: 0 (0.000%)
latency average = 11.904 ms
latency stddev = 8.986 ms
initial connection time = 15.370 ms
tps = 671.486782 (without initial connection time)
```

### Объем журнальных файлов
```
db=# SELECT pg_size_pretty(pg_wal_lsn_diff(pg_current_wal_lsn(), '0/0'));
 pg_size_pretty 
----------------
 518 MB
(1 row)
```

получается всего 518 MB

если точки раз в 30 сек, то должно быть 20 точек  
получается примерно 25 Mb на точку

__Однако размер файлов на диске всего 65Mb__
```
sudo du -sh dbdata/pg_wal
65M	dbdata/pg_wal
```

**Вопрос 2: я не совсем понимаю почему так происходит**


### Расписание контроольных точек
Всего получилось точек:
```
db=# select * from pg_stat_bgwriter;
-[ RECORD 1 ]---------+-----------------------------
checkpoints_timed     | 23
checkpoints_req       | 2
checkpoint_write_time | 566285
checkpoint_sync_time  | 394
buffers_checkpoint    | 44056
buffers_clean         | 0
maxwritten_clean      | 0
buffers_backend       | 4764
buffers_backend_fsync | 0
buffers_alloc         | 7304
stats_reset           | 2024-03-24 17:42:27.04136+00
```

23 - не совсем корректно,  
т.к. кол-во точек отсчитывается каждые 30 сек (постоянно),  
и я не сразу выполнил этот запрос после окончания нагрузки

Контрольные точки выполняли четко по рассписанию (из логов)
```shell
2024-03-24 17:45:23.041 UTC [28] LOG:  checkpoint complete: wrote 1834 buffers (11.2%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.558 s, sync=0.017 s, total=26.608 s; sync files=63, longest=0.006 s, average=0.001 s; distance=13011 kB, estimate=13011 kB
2024-03-24 17:45:53.044 UTC [28] LOG:  checkpoint complete: wrote 2018 buffers (12.3%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.958 s, sync=0.015 s, total=27.000 s; sync files=19, longest=0.008 s, average=0.001 s; distance=23836 kB, estimate=23836 kB
2024-03-24 17:46:23.052 UTC [28] LOG:  checkpoint complete: wrote 1927 buffers (11.8%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.962 s, sync=0.017 s, total=27.006 s; sync files=9, longest=0.007 s, average=0.002 s; distance=23985 kB, estimate=23985 kB
2024-03-24 17:46:53.058 UTC [28] LOG:  checkpoint complete: wrote 2129 buffers (13.0%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.954 s, sync=0.020 s, total=27.004 s; sync files=17, longest=0.012 s, average=0.002 s; distance=24357 kB, estimate=24357 kB
2024-03-24 17:47:23.054 UTC [28] LOG:  checkpoint complete: wrote 2058 buffers (12.6%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.962 s, sync=0.007 s, total=26.993 s; sync files=10, longest=0.004 s, average=0.001 s; distance=25342 kB, estimate=25342 kB
2024-03-24 17:47:53.061 UTC [28] LOG:  checkpoint complete: wrote 2119 buffers (12.9%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.978 s, sync=0.011 s, total=27.004 s; sync files=16, longest=0.006 s, average=0.001 s; distance=24511 kB, estimate=25259 kB
2024-03-24 17:48:23.061 UTC [28] LOG:  checkpoint complete: wrote 2031 buffers (12.4%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.961 s, sync=0.023 s, total=26.998 s; sync files=9, longest=0.015 s, average=0.003 s; distance=24652 kB, estimate=25198 kB
2024-03-24 17:48:53.054 UTC [28] LOG:  checkpoint complete: wrote 2095 buffers (12.8%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.965 s, sync=0.011 s, total=26.993 s; sync files=16, longest=0.007 s, average=0.001 s; distance=23846 kB, estimate=25063 kB
2024-03-24 17:49:23.066 UTC [28] LOG:  checkpoint complete: wrote 2011 buffers (12.3%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.963 s, sync=0.017 s, total=27.010 s; sync files=9, longest=0.010 s, average=0.002 s; distance=24132 kB, estimate=24970 kB
2024-03-24 17:49:53.061 UTC [28] LOG:  checkpoint complete: wrote 2093 buffers (12.8%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.959 s, sync=0.022 s, total=26.992 s; sync files=17, longest=0.013 s, average=0.002 s; distance=23919 kB, estimate=24865 kB
2024-03-24 17:50:23.062 UTC [28] LOG:  checkpoint complete: wrote 1999 buffers (12.2%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.961 s, sync=0.008 s, total=26.999 s; sync files=9, longest=0.005 s, average=0.001 s; distance=22768 kB, estimate=24655 kB
2024-03-24 17:50:53.063 UTC [28] LOG:  checkpoint complete: wrote 2088 buffers (12.7%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.967 s, sync=0.012 s, total=26.998 s; sync files=15, longest=0.009 s, average=0.001 s; distance=23816 kB, estimate=24571 kB
2024-03-24 17:51:23.074 UTC [28] LOG:  checkpoint complete: wrote 2019 buffers (12.3%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.961 s, sync=0.005 s, total=27.009 s; sync files=9, longest=0.005 s, average=0.001 s; distance=23821 kB, estimate=24496 kB
2024-03-24 17:51:53.060 UTC [28] LOG:  checkpoint complete: wrote 2095 buffers (12.8%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.959 s, sync=0.005 s, total=26.984 s; sync files=14, longest=0.005 s, average=0.001 s; distance=23784 kB, estimate=24425 kB
2024-03-24 17:52:23.076 UTC [28] LOG:  checkpoint complete: wrote 1998 buffers (12.2%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.966 s, sync=0.005 s, total=27.013 s; sync files=9, longest=0.005 s, average=0.001 s; distance=24076 kB, estimate=24390 kB
2024-03-24 17:52:53.090 UTC [28] LOG:  checkpoint complete: wrote 2330 buffers (14.2%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.969 s, sync=0.012 s, total=27.011 s; sync files=14, longest=0.005 s, average=0.001 s; distance=23116 kB, estimate=24263 kB
2024-03-24 17:53:23.097 UTC [28] LOG:  checkpoint complete: wrote 1998 buffers (12.2%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.969 s, sync=0.008 s, total=27.004 s; sync files=10, longest=0.008 s, average=0.001 s; distance=23951 kB, estimate=24232 kB
2024-03-24 17:53:53.116 UTC [28] LOG:  checkpoint complete: wrote 2059 buffers (12.6%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.955 s, sync=0.026 s, total=27.019 s; sync files=12, longest=0.014 s, average=0.003 s; distance=23577 kB, estimate=24166 kB
2024-03-24 17:54:23.098 UTC [28] LOG:  checkpoint complete: wrote 1972 buffers (12.0%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.956 s, sync=0.006 s, total=26.981 s; sync files=10, longest=0.006 s, average=0.001 s; distance=23856 kB, estimate=24135 kB
2024-03-24 17:54:53.125 UTC [28] LOG:  checkpoint complete: wrote 2322 buffers (14.2%); 0 WAL file(s) added, 0 removed, 2 recycled; write=26.957 s, sync=0.025 s, total=27.025 s; sync files=14, longest=0.015 s, average=0.002 s; distance=23782 kB, estimate=24100 kB
2024-03-24 17:55:23.034 UTC [28] LOG:  checkpoint complete: wrote 1983 buffers (12.1%); 0 WAL file(s) added, 0 removed, 1 recycled; write=26.874 s, sync=0.009 s, total=26.906 s; sync files=10, longest=0.009 s, average=0.001 s; distance=23072 kB, estimate=23997 kB
```

Как я понимаю, точки могли создаваться чаще, если был бы превышен лимит wal_max_size.  
Поскольку он не превышен, точки выполнялись каждые 30 сек.  

Я уменьшил wal_max_size и получил результат - точки стали создаваться чаще:  
```sh
2024-03-24 18:22:19.367 UTC [28] LOG:  checkpoint complete: wrote 1289 buffers (7.9%); 0 WAL file(s) added, 1 removed, 0 recycled; write=7.430 s, sync=0.030 s, total=7.490 s; sync files=18, longest=0.024 s, average=0.002 s; distance=10556 kB, estimate=10556 kB
2024-03-24 18:22:29.765 UTC [28] LOG:  checkpoint complete: wrote 1735 buffers (10.6%); 0 WAL file(s) added, 1 removed, 0 recycled; write=7.238 s, sync=0.020 s, total=7.299 s; sync files=7, longest=0.019 s, average=0.003 s; distance=16374 kB, estimate=16374 kB
2024-03-24 18:22:41.021 UTC [28] LOG:  checkpoint complete: wrote 1855 buffers (11.3%); 0 WAL file(s) added, 0 removed, 1 recycled; write=6.829 s, sync=0.047 s, total=6.913 s; sync files=15, longest=0.025 s, average=0.004 s; distance=16383 kB, estimate=16383 kB
2024-03-24 18:22:51.533 UTC [28] LOG:  checkpoint complete: wrote 1718 buffers (10.5%); 0 WAL file(s) added, 0 removed, 1 recycled; write=7.842 s, sync=0.019 s, total=7.886 s; sync files=7, longest=0.017 s, average=0.003 s; distance=16384 kB, estimate=16384 kB
2024-03-24 18:23:04.016 UTC [28] LOG:  checkpoint complete: wrote 1712 buffers (10.4%); 0 WAL file(s) added, 0 removed, 1 recycled; write=9.732 s, sync=0.012 s, total=9.770 s; sync files=9, longest=0.010 s, average=0.002 s; distance=16384 kB, estimate=16384 kB
```

## Синхронном/асинхронном режим
```
db=# SHOW synchronous_commit;
 synchronous_commit 
--------------------
 on
(1 row)
```
tps = 603.999736 (without initial connection time)


```
alter system set synchronous_commit = off;
```
tps = 2324.602765 (without initial connection time)


Асинхронный режим значительно быстрее.
Не требуется синхронная запись изменений на диск перед подтверждением транзакции.
Меньше нагрузка не диск.

## Контрольная сумма

Создал БД с включенной опцией data_checksums.  
Через docker это делается добавлением переменной среды:
```
POSTGRES_INITDB_ARGS: "--data-checksums"
```

Создаю таблицу:
```
create table person(name text);
insert into person (name) values ('person1'), ('person2');
```

Смотрю где файл данных:
```
db=# SELECT pg_relation_filepath('person');
 pg_relation_filepath 
----------------------
 base/16384/16389
(1 row)
```

Останавливаю БД и редактирую файл с данными (**меняю** несколько символов в конце).  

Выполняю чтение:
```
db=# select * from person;
WARNING:  page verification failed, calculated checksum 12574 but expected 53526
ERROR:  invalid page in block 0 of relation base/16384/16389
``` 
**Контрольная сумма не совпадает, чтение недоступно**

Пробую игнорировать:
```
set ignore_checksum_failure = on;
db=# select * from person;
WARNING:  page verification failed, calculated checksum 12574 but expected 53526
  name   
---------
 person +
 
 person2
(2 rows)
```
**ignore_checksum_failure позволяет читать, игнорируя несовпадение контрольной суммы**


### Почему не получилось в первый раз
Я думаю, что я зря удалял символы - нужно менять.  
В результате, видимо, файл менялся сильно, и запрос (select) возвращал пустую таблицу.  
Это повторяется стабильно.  